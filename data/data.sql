# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.41-0ubuntu0.14.04.1)
# Database: SupervillainHQ
# Generation Time: 2015-03-21 12:52:53 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table Cultures
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Cultures`;

CREATE TABLE `Cultures` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Files
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Files`;

CREATE TABLE `Files` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filepath` varchar(255) NOT NULL DEFAULT '',
  `status` enum('deleted','locked','file') NOT NULL DEFAULT 'file',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table IframeSections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `IframeSections`;

CREATE TABLE `IframeSections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pagesection_id` int(11) unsigned NOT NULL,
  `src` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `pagesection_fk` (`pagesection_id`),
  CONSTRAINT `IframeSections_ibfk_1` FOREIGN KEY (`pagesection_id`) REFERENCES `PageSections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Languages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Languages`;

CREATE TABLE `Languages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `language` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Languages` WRITE;
/*!40000 ALTER TABLE `Languages` DISABLE KEYS */;

INSERT INTO `Languages` (`id`, `language`)
VALUES
	(1,'english'),
	(2,'danish');

/*!40000 ALTER TABLE `Languages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Pages`;

CREATE TABLE `Pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `slug` varchar(128) NOT NULL DEFAULT '',
  `status` enum('draft','published','deleted') NOT NULL DEFAULT 'draft',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Pages` WRITE;
/*!40000 ALTER TABLE `Pages` DISABLE KEYS */;

INSERT INTO `Pages` (`id`, `title`, `slug`, `status`, `created`, `updated`)
VALUES
	(1,'About','about','draft','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(2,'Labs','labs','draft','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(3,'PhpBasics','phpbasics','draft','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(4,'PhpBasicsCms','phpbasics-cms','draft','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(5,'Php Ant Tasks','php-ant-tasks','draft','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(6,'Passwordhasher','passwordhasher','draft','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(7,'Wordpress CMS','wordpress-cms','draft','0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `Pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table PageSections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `PageSections`;

CREATE TABLE `PageSections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `title` varchar(64) NOT NULL DEFAULT '',
  `status` enum('draft','published','deleted') NOT NULL DEFAULT 'draft',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `page_fk` (`page_id`),
  KEY `language_fk` (`language_id`),
  CONSTRAINT `PageSections_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `Pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PageSections_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `Languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `PageSections` WRITE;
/*!40000 ALTER TABLE `PageSections` DISABLE KEYS */;

INSERT INTO `PageSections` (`id`, `page_id`, `language_id`, `title`, `status`, `created`, `updated`)
VALUES
	(1,1,1,'pageHtmlContent','draft','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(2,2,1,'pageHtmlContent','draft','0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `PageSections` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table PageTextContent
# ------------------------------------------------------------

DROP VIEW IF EXISTS `PageTextContent`;

CREATE TABLE `PageTextContent` (
   `page_title` VARCHAR(128) NULL DEFAULT NULL,
   `page_slug` VARCHAR(128) NOT NULL DEFAULT '',
   `page_status` ENUM('draft','published','deleted') NOT NULL DEFAULT 'draft',
   `page_created` DATETIME NOT NULL,
   `page_updated` DATETIME NOT NULL,
   `section_status` ENUM('draft','published','deleted') NULL DEFAULT 'draft',
   `language_id` INT(11) UNSIGNED NULL DEFAULT '0',
   `language` VARCHAR(64) NULL DEFAULT '',
   `section_title` VARCHAR(64) NULL DEFAULT '',
   `section_created` DATETIME NOT NULL,
   `section_updated` DATETIME NOT NULL,
   `textcontent` MEDIUMTEXT NULL DEFAULT NULL
) ENGINE=MyISAM;



# Dump of table PhpSections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `PhpSections`;

CREATE TABLE `PhpSections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pagesection_id` int(11) unsigned NOT NULL,
  `filepath` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `pagesection_fk` (`pagesection_id`),
  CONSTRAINT `PhpSections_ibfk_1` FOREIGN KEY (`pagesection_id`) REFERENCES `PageSections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table TextSections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `TextSections`;

CREATE TABLE `TextSections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pagesection_id` int(11) unsigned NOT NULL,
  `textcontent` mediumtext,
  PRIMARY KEY (`id`),
  KEY `pagesection_fk` (`pagesection_id`),
  CONSTRAINT `TextSections_ibfk_1` FOREIGN KEY (`pagesection_id`) REFERENCES `PageSections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `TextSections` WRITE;
/*!40000 ALTER TABLE `TextSections` DISABLE KEYS */;

INSERT INTO `TextSections` (`id`, `pagesection_id`, `textcontent`)
VALUES
	(1,1,'This is the text content for Page #1'),
	(2,2,'This is the text for page #2');

/*!40000 ALTER TABLE `TextSections` ENABLE KEYS */;
UNLOCK TABLES;




# Replace placeholder table for PageTextContent with correct view syntax
# ------------------------------------------------------------

DROP TABLE `PageTextContent`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `PageTextContent`
AS SELECT
   `p`.`title` AS `page_title`,
   `p`.`slug` AS `page_slug`,
   `p`.`status` AS `page_status`,
   `p`.`created` AS `page_created`,
   `p`.`updated` AS `page_updated`,
   `ps`.`status` AS `section_status`,
   `l`.`id` AS `language_id`,
   `l`.`language` AS `language`,
   `ps`.`title` AS `section_title`,
   `p`.`created` AS `section_created`,
   `p`.`updated` AS `section_updated`,
   `ts`.`textcontent` AS `textcontent`
FROM (((`Pages` `p` left join `PageSections` `ps` on((`p`.`id` = `ps`.`page_id`))) left join `Languages` `l` on((`ps`.`language_id` = `l`.`id`))) left join `TextSections` `ts` on((`ts`.`pagesection_id` = `ps`.`id`))) where (`ts`.`textcontent` is not null);

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
