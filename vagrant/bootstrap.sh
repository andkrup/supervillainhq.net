#!/usr/bin/env bash

if [[ -d /vagrant/.apache ]]
	then
	rm -rf /vagrant/.apache
fi
if [[ -d /vagrant/.xdebug ]]
	then
	rm -rf /vagrant/.xdebug
fi
mkdir -p /vagrant/.xdebug /vagrant/.apache

locale-gen en_US.UTF-8
# fix missing locales for ssh session
sed -i '/AcceptEnv LANG LC_*/c\#AcceptEnv LANG LC_*' /etc/ssh/sshd_config
#echo 'export LC_ALL="en_US.UTF-8"' >> /home/vagrant/.bashrc
#echo 'export LANGUAGE="en_US.UTF-8"' >> /home/vagrant/.bashrc
#echo 'export LANG="en_US.UTF-8"' >> /home/vagrant/.bashrc
#echo 'export LC_MESSAGES="en_US.UTF-8"' >> /home/vagrant/.bashrc

# add required repos (phalcon)
#apt-get install python-software-properties
apt-add-repository ppa:phalcon/stable -y
apt-add-repository ppa:chris-lea/redis-server -y

apt-get update

# set mysql root password for scripted mysql install
debconf-set-selections <<< 'mysql-server mysql-server/root_password password vagrant'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password vagrant'

# download and install required software
apt-get install apache2 php5 git php5-xdebug php5-curl php5-mysql php5-phalcon mysql-server default-jdk redis-server nodejs npm -y

# disable xdebug in php cli
if [[ -L /etc/php5/cli/conf.d/20-xdebug.ini ]]
	then
	echo "disabling php-xdebug in cli-mode"
	unlink /etc/php5/cli/conf.d/20-xdebug.ini
fi

# download & install composer
#cd /usr/local/bin
if [[ ! -f /usr/local/bin/composer ]]
	then
	echo "downloading & installing composer"
	curl -sS https://getcomposer.org/installer | php -- --install-dir=/home/vagrant
	mv /home/vagrant/composer.phar /usr/local/bin/composer
	chmod +x /usr/local/bin/composer
fi

# fix missing node symlink
if [[ ! -L /usr/local/bin/node ]]
	then
	echo "updating node symlink"
	ln -s /usr/bin/nodejs /usr/local/bin/node
fi
# install bower, gulp & stylus
if [[ ! -f /usr/local/bin/bower ]]
	then
	echo "downloading & installing bower, gulp & stylus"
	npm install -g bower gulp stylus
	# set up gulp in project
	cd /var/www/supervillainhq.net/
	npm install --save-dev gulp gulp-stylus
fi

# enable apache modules
a2enmod rewrite

# replace /var/log/apache2 with symlink to vagrant folder
rm -rf /var/log/apache2
ln -fs /vagrant/.apache /var/log/apache2

# symlink apache configurations to the sites-enabled folder
ln -s /var/www/supervillainhq.net/env/vagrant/supervillainhq.dev.conf 000-supervillainhq.dev.conf

# add our custom php settings
ln -fs /vagrant/php.ini /etc/php5/apache2/conf.d/customsettings.ini


# install composer dependencies
cd /var/www/supervillainhq.net
composer install

# install bower components
bower install

# re-establish database with user @ 'localhost'

service apache2 restart
