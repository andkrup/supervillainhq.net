grant all privileges on *.* to 'root'@'%' with grant option;
flush privileges;
create database if not exists SupervillainHQ default character set utf8 default collate utf8_general_ci;
grant all privileges on SupervillainHQ.* to 'supervillain'@'localhost';
set password for 'supervillain'@'localhost' = password('supervillain');
