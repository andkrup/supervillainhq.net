<?php
# composer autoload
include __DIR__ . '/../vendor/autoload.php';

use supervillainhq\spectre\db\SqlQuery;
use supervillainhq\core\date\Date;
error_reporting(E_ALL);


try {

    /**
     * Read the configuration
     */
    $config = include __DIR__ . "/../config/config.php";

    /**
     * Read auto-loader
     */
    include __DIR__ . "/../config/loader.php";

    /**
     * Read services
     */
    include __DIR__ . "/../config/services.php";

    /**
     * Handle the request
     */
    $application = new \Phalcon\Mvc\Application($di);

	$application->registerModules(
		[
			'frontend' => [
				'className' => 'supervillainhq\www\HeadQuartersFrontend',
				'path'      => '../app/src/supervillainhq/www/HeadQuartersFrontend.php',
			],
// 			'api'  => [
// 				'className' => 'supervillainhq\www\api\HeadQuartersAPI',
// 				'path'      => '../app/src/supervillainhq/www/api/HeadQuartersAPI.php',
// 			],
			'admin'  => [
				'className' => 'supervillainhq\www\admin\HeadQuartersAdmin',
				'path'      => '../app/src/supervillainhq/www/admin/HeadQuartersAdmin.php',
			]
		]
	);
    // static reference to the current dbadapter
    SqlQuery::$dbAdapter = $application->db;
    // set up datetimeutil
    Date::config((array) $config->locale);

    echo $application->handle()->getContent();

} catch (\Exception $e) {
    echo $e->getMessage();
}
