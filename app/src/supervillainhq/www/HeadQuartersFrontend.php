<?php
namespace supervillainhq\www{
	use Phalcon\Mvc\ModuleDefinitionInterface;
	use Phalcon\Mvc\View;
	use Phalcon\Mvc\Dispatcher;
	use Phalcon\DiInterface;
	use Phalcon\Loader;

	class HeadQuartersFrontend implements ModuleDefinitionInterface{

		public function registerAutoloaders(DiInterface $dependencyInjector=null){
			$loader = new Loader();
			$loader->registerNamespaces([
					'controllers' => '../app/frontend/controllers/',
			]);
			$loader->register();
		}
		public function registerServices(DiInterface $dependencyInjector){
			//Registering a dispatcher
			$dependencyInjector->set('dispatcher', function() {
				$dispatcher = new Dispatcher();
				$dispatcher->setDefaultNamespace("controllers");
				return $dispatcher;
			});

			//Registering the view component
			$dependencyInjector->set('view', function() {
				$view = new View();
				$view->setViewsDir('../app/frontend/views/');
				return $view;
			});
		}

	}
}