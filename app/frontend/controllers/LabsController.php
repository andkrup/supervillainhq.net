<?php
namespace controllers{
	use supervillainhq\spectre\cms\controllers\CmsController;

	class LabsController extends CmsController{
		function index(){

			$pageContent = <<<CONTENT
CONTENT;
			$this->data = [
					'pageHtmlContent' => $pageContent,
					'siteHtmlFooter' => '<p>&copy; supervillainhq.net 2015</p>',
					'authed' => false,
			];
		}

		function microshopAction(){
		}

		function phpbasicsAction(){
		}

		function phphalconcmsAction(){
		}

		function phpAntTasks(){
			$this->loadTemplate('index.html');

			$pageContent = <<<CONTENT
<article>
	<h2>Php Ant Tasks</h2>
	<p>Evil and resourceful web masterminds can now use these specialised Ant tasks that will help deploy and control web sites</p>
</article>
CONTENT;
			$this->data = [
					'pageHtmlContent' => $pageContent,
					'siteHtmlFooter' => '<p>&copy; supervillainhq.net 2015</p>',
					'authed' => false,
			];
		}

		function passwordhasher(){
			$this->loadTemplate('index.html');

			$pageContent = <<<CONTENT
<article>
	<h2>Php Ant Tasks</h2>
	<p>A passwordh hashing CLI-tool that can produce a unique 32-character long password for every web site where you&#34;re have an account.</p>
</article>
CONTENT;
			$this->data = [
					'pageHtmlContent' => $pageContent,
					'siteHtmlFooter' => '<p>&copy; supervillainhq.net 2015</p>',
					'authed' => false,
			];
		}

		function wordpressCms(){
			$this->loadTemplate('index.html');

			$pageContent = <<<CONTENT
<article>
	<h2>Wordpress CMS</h2>
	<p>A Wordpress plugin that transforms the blogging tool into a customisable Content Managing System.</p>
</article>
CONTENT;
			$this->data = [
					'pageHtmlContent' => $pageContent,
					'siteHtmlFooter' => '<p>&copy; supervillainhq.net 2015</p>',
					'authed' => false,
			];
		}
	}
}