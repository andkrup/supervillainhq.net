var gulp = require('gulp');
var config = require('./gulp.json');
var stylus = require('gulp-stylus');
var replace = require('gulp-replace');
var rsync = require('gulp-rsync');

gulp.task('dependencies', function () {
	gulp.src(config.bowerpath + '/jquery/dist/jquery.min.js').pipe(gulp.dest('public/js/'));
	gulp.src(config.bowerpath + '/mustache.js/mustache.min.js').pipe(gulp.dest('public/js/'));
	gulp.src(config.bowerpath + '/cookieconsent2/build/cookieconsent.min.js').pipe(gulp.dest('public/js/'));
	
	gulp.src(config.bowerpath + '/HTML5-Reset/assets/css/reset.css').pipe(gulp.dest('public/css/'));
	gulp.src(config.bowerpath + '/cookieconsent2/build/light-bottom.css').pipe(gulp.dest('public/css/'));
	gulp.src(config.frontendpath + '/fonts.css').pipe(gulp.dest('public/css/'));
});

gulp.task('styles', function () {
	gulp.src(config.styluspath + '/screen.styl').pipe(stylus()).pipe(gulp.dest('./public/css'));
});