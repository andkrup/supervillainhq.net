<?php

use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Router;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Session as FlashSession;
use supervillainhq\core\ObjectEncoder;
use supervillainhq\core\DataObjectFactory;
use supervillainhq\henchmen\gettext\Gettext;
use supervillainhq\spectre\auth\MySqlAuthenticator;
use supervillainhq\spectre\auth\db\AuthUserMapper;
use supervillainhq\spectre\cms\CmsClient;
use supervillainhq\spectre\cms\PageSectionManagerFactory;
use supervillainhq\spectre\cms\Site;
use supervillainhq\spectre\cms\assets\UploadManager;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

$di->set('config', function () use ($config) {
	return $config;
}, true);

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
}, true);

/**
 * Setting up the view component
 */
$di->set('view', function () use ($config) {

    $view = new View();

    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines([
        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
    ]);

    return $view;
}, true);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function () use ($config) {
	return new DbAdapter(array(
		'host' => $config->database->host,
		'username' => $config->database->username,
		'password' => $config->database->password,
		'dbname' => $config->database->dbname,
		"charset" => $config->database->charset
	));
});

$di->set('objectmapper', function ($type) use($di){
	$parameters = func_get_args();
	array_shift($parameters);
	$map = include dirname(__FILE__) . '/classmap.php';
	$factory = new DataObjectFactory($map, $di);
	return $factory->getMapper($type, $parameters);
});
$di->set('objectwriter', function ($type) use($di){
	$parameters = func_get_args();
	array_shift($parameters);
	$map = include dirname(__FILE__) . '/classmap.php';
	$factory = new DataObjectFactory($map, $di);
	return $factory->getWriter($type, $parameters);
});

$di->set('router', function () {
	$router = new Router();
	$router->setDefaultModule("frontend");
	$router->add(
			'/:action', [
					'module' => 'frontend',
					'controller' => 'index',
					"action"     => 1
			]
	);
	$router->add(
			'/:controller/:action/:params', [
					'module' => 'frontend',
					'controller' => 1,
					"action"     => 2,
					"params"     => 3,
			]
	);
	$router->add(
			'/admin/:params',
			[
					'module' => 'admin',
					'controller' => 'index'
			]
	);
	$router->add(
			"/admin/:action/:params",
			[
					"module" => "admin",
					"controller" => 1,
					"action"     => 'index',
					"params"     => 2,
			]
	);
	$router->add(
			"/admin/:controller/:action/:params",
			[
					"module" => "admin",
					"controller" => 1,
					"action"     => 2,
					"params"     => 3,
			]
	);
// 	$router->add(
// 			"/api/:controller/:action/:params",
// 			[
// 					"module" => "api",
// 					"controller" => 1,
// 					"action"     => 2,
// 					"params"     => 3,
// 			]
// 	);
	return $router;
}, true);

$di->set('dispatcher', function() use ($di) {
	$eventsManager = $di->getShared('eventsManager');
	$fishingJournal = $di->get('fishingjournal');
	$eventsManager->attach('dispatch', $fishingJournal);
	$client = $di->get('cmsclient');
	$eventsManager->attach('application', $client);
	$eventsManager->attach('dispatch', $client);
	$dispatcher = new Dispatcher();
	$dispatcher->setEventsManager($eventsManager);
	return $dispatcher;
});

// Set up the flash service
$di->set('flash', function () {
    return new FlashSession();
});

// Frontend client object
$di->set('cmsclient', function() use($config){
	$cmsClient = new CmsClient();
	return $cmsClient;
}, true);
// Ajax Api-client object
$di->set('ajaxapi', function(){
	return AjaxFishingJournalApi::instance();
}, true);
// A factory that produces PageSectionManagers (objects that contains the logic for how pages should display sections)
$di->set('sectionManagerFactory', function() use($config){
	$factory = new PageSectionManagerFactory();
	return $factory;
}, true);

// scripts/styles list
$di->set('site', function() {
	return Site::instance();
}, true);

// json encoder
$di->set('objectencoder', function($value) use($di){
	return new ObjectEncoder($di, $value);
}, true);
$di->set('encoderfactory', function($value){
	$factory = new FishingJournalEncoderFactory($value);
	return $factory->create();
});

// Auth helper
$di->set('auth', function() use ($di){
	$security = $di->get('security');
	$session = $di->get('session');
	return new MySqlAuthenticator($security, $session, $di);
}, true);
// User factory
$di->set('user', function(array $parameters) use ($di){
	return new AuthUserMapper($parameters);
}, true);

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->set('modelsMetadata', function () {
    return new MetaDataAdapter();
});


$di->set('uploader', function() use ($di, $config){
	return new UploadManager($config->files);
});

$di->set('gettext', function() use ($di, $config){
	return new Gettext($config->gettext, $config->localization);
});

/**
 * Hook handler
 */
$di->set('smee', function() use ($di, $config){
	return new Smee($config->smee, $config->localization);
});


/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
	$session = new SessionAdapter([
			'uniqueId' => '2-bissen-web'
	]);
	$session->start();

	return $session;
});
