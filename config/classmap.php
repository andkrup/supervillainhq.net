<?php
return [
		'mappers' => [
				'asset' => 'supervillainhq\phalcon\cms\db\AssetMapper',
				'product' => 'supervillainhq\tobissen\db\TobissenWoblerMapper',
				'category' => 'supervillainhq\phalcon\microshop\browsing\db\ProductCategoryMapper',
				'tag' => 'supervillainhq\phalcon\microshop\browsing\db\TagMapper',
				'invoice' => 'supervillainhq\phalcon\microshop\db\InvoiceMapper',
				'transaction' => 'supervillainhq\phalcon\microshop\db\TransactionMapper',
		],
		'writers' => [
				'asset' => 'supervillainhq\phalcon\cms\db\AssetWriter',
				'product' => 'supervillainhq\tobissen\db\TobissenWoblerWriter',
				'category' => 'supervillainhq\phalcon\microshop\browsing\db\ProductCategoryWriter',
				'tag' => 'supervillainhq\phalcon\microshop\browsing\db\TagWriter',
				'invoice' => 'supervillainhq\phalcon\microshop\db\InvoiceWriter',
				'transaction' => 'supervillainhq\phalcon\microshop\db\TransactionWriter',
		]
];